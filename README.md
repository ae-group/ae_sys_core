<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.90 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.12 -->
# sys_core 0.3.23

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_sys_core/develop?logo=python)](
    https://gitlab.com/ae-group/ae_sys_core)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_sys_core/release0.3.22?logo=python)](
    https://gitlab.com/ae-group/ae_sys_core/-/tree/release0.3.22)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_sys_core)](
    https://pypi.org/project/ae-sys-core/#history)

>ae namespace module portion sys_core: dynamic system configuration, initialization and connection.

[![Coverage](https://ae-group.gitlab.io/ae_sys_core/coverage.svg)](
    https://ae-group.gitlab.io/ae_sys_core/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_sys_core/mypy.svg)](
    https://ae-group.gitlab.io/ae_sys_core/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_sys_core/pylint.svg)](
    https://ae-group.gitlab.io/ae_sys_core/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_sys_core)](
    https://gitlab.com/ae-group/ae_sys_core/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_sys_core)](
    https://gitlab.com/ae-group/ae_sys_core/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_sys_core)](
    https://gitlab.com/ae-group/ae_sys_core/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_sys_core)](
    https://pypi.org/project/ae-sys-core/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_sys_core)](
    https://gitlab.com/ae-group/ae_sys_core/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_sys_core)](
    https://libraries.io/pypi/ae-sys-core)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_sys_core)](
    https://pypi.org/project/ae-sys-core/#files)


## installation


execute the following command to install the
ae.sys_core module
in the currently active virtual environment:
 
```shell script
pip install ae-sys-core
```

if you want to contribute to this portion then first fork
[the ae_sys_core repository at GitLab](
https://gitlab.com/ae-group/ae_sys_core "ae.sys_core code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_sys_core):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_sys_core/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.sys_core.html
"ae_sys_core documentation").
